package com.gabchak.view;

public class ViewImpl implements View {

    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RESET = "\u001B[0m";

    @Override
    public void printMessage(String message) {
        System.out.println(message);
    }

    @Override
    public void printYellowMessage(String message) {
        System.out.print(ANSI_YELLOW + message + ANSI_RESET);
    }

    @Override
    public void printGreenMessage(String message) {
        System.out.print(ANSI_GREEN + message + ANSI_RESET);
    }

    @Override
    public void printRedMessage(String message) {
        System.out.println(ANSI_RED + message + ANSI_RESET);
    }

}