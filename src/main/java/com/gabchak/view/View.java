package com.gabchak.view;

public interface View {

    void printMessage(String message);

    void printYellowMessage(String message);

    void printGreenMessage(String message);

    void printRedMessage(String message);
}
