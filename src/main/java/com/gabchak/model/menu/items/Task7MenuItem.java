package com.gabchak.model.menu.items;

import com.gabchak.model.tasks.task7.PipeCommunicate;
import com.gabchak.view.View;

import java.io.IOException;

import static java.lang.String.format;

public class Task7MenuItem extends AbstractMenuItem {
    private static final String NAME = "Pipe communicate";
    private static final String DETAILS = "";

    public Task7MenuItem(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {
        try {
            new PipeCommunicate().start(view);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
