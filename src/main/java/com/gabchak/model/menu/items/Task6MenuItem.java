package com.gabchak.model.menu.items;

import com.gabchak.model.tasks.task6.SynchronizedMethodsSameObject;
import com.gabchak.view.View;

import static java.lang.String.format;

public class Task6MenuItem extends AbstractMenuItem {
    private static final String NAME = "Synchronized methods same object";
    private static final String DETAILS = "";

    public Task6MenuItem(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {
        try {
            new SynchronizedMethodsSameObject().start(view);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
