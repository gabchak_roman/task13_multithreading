package com.gabchak.model.menu.items;

import com.gabchak.model.tasks.task6.SyncMethodsDifferentObjects;
import com.gabchak.model.tasks.task6.SynchronizedLock;
import com.gabchak.view.View;

import static java.lang.String.format;

public class Task6_3_Menuitem extends AbstractMenuItem {
    private static final String NAME = "Synchronized";
    private static final String DETAILS = "'ReentrantLock'";

    public Task6_3_Menuitem(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {
        try {
            new SynchronizedLock().start(view);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
