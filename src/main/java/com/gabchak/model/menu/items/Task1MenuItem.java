package com.gabchak.model.menu.items;

import com.gabchak.model.tasks.task1.PingPong;
import com.gabchak.view.View;

import static java.lang.String.format;

public class Task1MenuItem extends AbstractMenuItem {
    private static final String NAME = "Ping-pong";
    private static final String DETAILS = "'wait() and notify'";

    public Task1MenuItem(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {
        new PingPong().playPingPong(view);
    }
}
