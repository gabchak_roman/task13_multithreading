package com.gabchak.model.menu.items;

import com.gabchak.model.tasks.task5.SleepTask;
import com.gabchak.view.ConsoleReader;
import com.gabchak.view.View;

import static java.lang.String.format;

public class Task5MenuItem extends AbstractMenuItem {
    private static final String NAME = "Sleep threads";
    private static final String DETAILS = "'ScheduledThreadPool'";

    private final ConsoleReader consoleReader;

    public Task5MenuItem(String key, String title, View view, ConsoleReader consoleReader) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);

        this.consoleReader = consoleReader;
    }

    @Override
    public void execute() {
        view.printYellowMessage("\n Enter number of tasks: ");
        int corePoolSize = Integer.parseInt(consoleReader.readLine());
        try {
            new SleepTask().start(view, corePoolSize);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
