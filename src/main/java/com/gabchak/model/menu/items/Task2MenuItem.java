package com.gabchak.model.menu.items;

import com.gabchak.model.tasks.task2.StartThreads;
import com.gabchak.view.View;

import static java.lang.String.format;

public class Task2MenuItem extends AbstractMenuItem {
    private static final String NAME = "Fibonacci sequence";
    private static final String DETAILS = "'threads'";

    public Task2MenuItem(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {
        try {
            new StartThreads(view, NUMBER_OF_THREADS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
