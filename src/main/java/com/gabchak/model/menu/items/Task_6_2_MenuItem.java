package com.gabchak.model.menu.items;

import com.gabchak.model.tasks.task6.SyncMethodsDifferentObjects;
import com.gabchak.view.View;

import static java.lang.String.format;

public class Task_6_2_MenuItem extends AbstractMenuItem {
    private static final String NAME = "Synchronized methods different object";
    private static final String DETAILS = "";

    public Task_6_2_MenuItem(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {
        try {
            new SyncMethodsDifferentObjects().start(view);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
