package com.gabchak.model.menu.items;

import com.gabchak.model.tasks.task4.StartFibonacciCallable;
import com.gabchak.view.View;

import java.util.concurrent.ExecutionException;

import static java.lang.String.format;

public class Task4MenuItem extends AbstractMenuItem {
    private static final String NAME = "Fibonacci sequence sum";
    private static final String DETAILS = "'callable'";

    public Task4MenuItem(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {
        try {
            new StartFibonacciCallable().printFibSum(view, NUMBER_OF_THREADS);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
