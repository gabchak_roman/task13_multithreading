package com.gabchak.model.menu.items;

import com.gabchak.model.tasks.task7.BlockingQueueSync;
import com.gabchak.view.View;

import static java.lang.String.format;

public class Task_7_1_MenuItem extends AbstractMenuItem{
    private static final String NAME = "Synchronized";
    private static final String DETAILS = "'BlockingQueue'";

    public Task_7_1_MenuItem(String key, String title, View view) {
        super(key,
                format(TITLE_FORMAT, title, NAME, DETAILS),
                view);
    }

    @Override
    public void execute() {
        try {
            new BlockingQueueSync().start(view);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
