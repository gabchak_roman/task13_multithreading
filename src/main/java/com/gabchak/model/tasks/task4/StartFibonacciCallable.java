package com.gabchak.model.tasks.task4;

import com.gabchak.model.tasks.task2.Fibonacci;
import com.gabchak.view.View;

import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class StartFibonacciCallable {

    public void printFibSum(View view, int numOfThreads)
            throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(numOfThreads);
        for (int i = 0; i < numOfThreads; i++) {
            view.printMessage(
                    executorService.submit(
                            new Fibonacci(
                                    new Random().nextInt(40))).get());
        }
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);
    }
}
