package com.gabchak.model.tasks.task2;

import com.gabchak.view.View;

import java.util.Random;

public class StartThreads {
    public StartThreads(View view, int numberOfThreads)
            throws InterruptedException {
        for (int i = 0; i < numberOfThreads; i++) {
            Thread thread = new Thread(() -> view.printMessage(
                    Thread.currentThread().getName() + " -> \n"
                            + (new Fibonacci(
                                    new Random().nextInt(40)).toString())));
            thread.start();
            thread.join();
        }
    }
}
