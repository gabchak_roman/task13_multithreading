package com.gabchak.model.tasks.task2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class Fibonacci implements Callable<String> {

    private int number;
    private List<Integer> sequence;

    public Fibonacci(int number) {
        this.number = number;
        fibN();
    }

    private void fibN() {
        List<Integer> fib = new ArrayList<>();
        int i;
        if (number >= 1){
            fib.add(0);
        }
        if (number >= 2){
            fib.add(1);
        }
        for (i = 2; i < number; i++) {
            fib.add(fib.get(i - 1) + fib.get(i - 2));
        }
        sequence = fib;
    }

    @Override
    public String toString() {
        return "Fibonacci number: " + number
                + "\n        sequence: " + sequence;
    }

    @Override
    public String call()  {

        return toString() +
                "\n            sum : " + sequence.stream().mapToInt(Integer::intValue).sum();
    }
}
