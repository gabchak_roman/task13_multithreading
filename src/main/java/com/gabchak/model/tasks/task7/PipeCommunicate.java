package com.gabchak.model.tasks.task7;

import com.gabchak.view.View;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

public class PipeCommunicate {

    public void start(View view) throws IOException, InterruptedException {
        final PipedOutputStream output = new PipedOutputStream();
        final PipedInputStream  input  = new PipedInputStream(output);

        Thread thread1 = new Thread(() -> {
            try {
                view.printYellowMessage(
                        "PipedOutputStream write the text: 'Hello world!'\n");
                output.write("Hello world!".getBytes());
            } catch (IOException ignored) {
            }
        });


        Thread thread2 = new Thread(() -> {
            try {
                int data = input.read();
                view.printYellowMessage(
                        "PipedInputStream read the text: ");
                while(data != -1){
                    view.printGreenMessage(String.valueOf((char) data));
                    data = input.read();
                }
            } catch (IOException ignored) {
            }
        });

        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
    }
}