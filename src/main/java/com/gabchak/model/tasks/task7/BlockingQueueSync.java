package com.gabchak.model.tasks.task7;

import com.gabchak.view.View;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class BlockingQueueSync {

    public void start(View view) throws InterruptedException {
        BlockingQueue<String> strings = new ArrayBlockingQueue<>(3);
        view.printYellowMessage("--------------- BlockingQueue -------------\n\n");
        Thread thread1 = new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                try {
                    Thread.sleep(500);
                    strings.put((i) + " Hello world!");
                    view.printYellowMessage("Put string: " + i + " 'Hello world!'\n");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread thread2 = new Thread(() -> {
            for (int i = 1; i <= 10; i++) {
                try {
                    Thread.sleep(510);
                    view.printRedMessage("Get string: " + strings.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();

    }
}