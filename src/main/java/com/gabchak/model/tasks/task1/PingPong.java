package com.gabchak.model.tasks.task1;

import com.gabchak.view.View;

import java.util.concurrent.atomic.AtomicInteger;

public class PingPong {

    private volatile AtomicInteger counter = new AtomicInteger(10);

    public void playPingPong(View view) {
        view.printMessage("--- Start ---");
        Thread thread1 = new Thread(() -> {
            synchronized (this) {
                while ((counter.decrementAndGet()) >= 0) {
                    try {
                        this.wait();
                        Thread.sleep(900);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    view.printYellowMessage("ping    ");
                    this.notify();
                }
            }
        });

        Thread thread2 = new Thread(() -> {
            while ((counter.decrementAndGet()) >= 0) {
                synchronized (this) {
                    this.notify();
                    try {
                        this.wait();
                        Thread.sleep(900);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    view.printGreenMessage("pong\n");
                }
            }
        });

        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        view.printMessage("--- End ---");
    }
}
