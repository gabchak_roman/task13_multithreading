package com.gabchak.model.tasks.task5;

import com.gabchak.view.View;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SleepTask {

    public void start(View view, int corePoolSize) throws InterruptedException {
        ExecutorService executorService = Executors.newScheduledThreadPool(corePoolSize);
        for (int i = 0; i < corePoolSize; i++) {
            executorService.submit(() -> {
                int timeSec = new Random().nextInt(10) + 1;
                try {
                    Thread.sleep(timeSec * 1000);
                    view.printMessage(Thread.currentThread().getName() + " -> "
                            + "sleep for a:    "+ timeSec + " sec");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
        }
        executorService.shutdown();
        executorService.awaitTermination(3, TimeUnit.MINUTES);
    }
}
