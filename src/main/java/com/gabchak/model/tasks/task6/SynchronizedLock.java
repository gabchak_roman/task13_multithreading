package com.gabchak.model.tasks.task6;

import com.gabchak.view.View;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class SynchronizedLock {
    private final Lock locker = new ReentrantLock();
    private int number = 0;

    public void start(View view) throws InterruptedException {
        ExecutorService executorService = Executors.newScheduledThreadPool(4);
        for (int i = 0; i < 1000; i++) {
            executorService.submit(() -> method1(view));
            executorService.submit(() -> method2(view));
            executorService.submit(() -> method3(view));
        }
        executorService.shutdown();
        executorService.awaitTermination(3, TimeUnit.MINUTES);
    }

    private void method1(View view) {
        locker.lock();
        try {
            for (int i = 0; i < 5; i++) {
                view.printMessage(Thread.currentThread().getName() + " ->   Method 1 :   "
                        + number + " + 1 = " + (number += 1));

            }
        } finally {
            locker.unlock();
        }
    }

    private void method2(View view) {
        locker.lock();
        try {
            for (int i = 0; i < 5; i++) {
                view.printMessage(Thread.currentThread().getName() + " ->   Method 2 :   "
                        + number + " + 2 = " + (number += 2));
            }
        } finally {
            locker.unlock();
        }
    }

    private void method3(View view) {
        locker.lock();
        try {
            for (int i = 0; i < 5; i++) {
                view.printMessage(Thread.currentThread().getName() + " ->   Method 3 :   "
                        + number + " + 3 = " + (number += 3));
            }
        } finally {
            locker.unlock();
        }
    }
}
