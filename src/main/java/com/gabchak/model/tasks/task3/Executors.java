package com.gabchak.model.tasks.task3;

import com.gabchak.model.tasks.task2.Fibonacci;
import com.gabchak.view.View;

import java.util.Random;
import java.util.concurrent.*;

public class Executors {

    private int numberOfThreads;

    public Executors(int numberOfThreads) {
        if (numberOfThreads <= 0) {
            numberOfThreads = 8;
        }
        this.numberOfThreads = numberOfThreads;
    }

    public void startExecutorService(View view) throws InterruptedException {
        ExecutorService executorService = java.util.concurrent.Executors.
                newFixedThreadPool(numberOfThreads);
        view.printYellowMessage("FixedThreadPool\n");
        for (int i = 0; i < numberOfThreads; i++) {
            executorService.submit(new MyRunnable(view));
        }
        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);

        view.printYellowMessage("SingleThreadExecutor\n");
        ExecutorService executorService2 = java.util.concurrent.Executors.
                newSingleThreadExecutor();
        for (int i = 0; i < numberOfThreads; i++) {
            executorService2.submit(new MyRunnable(view));
        }
        executorService2.shutdown();
        executorService2.awaitTermination(10, TimeUnit.SECONDS);

        view.printYellowMessage("CachedThreadPool\n");
        ExecutorService executorService3 = java.util.concurrent.Executors.
                newCachedThreadPool();
        for (int i = 0; i < numberOfThreads; i++) {
            executorService3.submit(new MyRunnable(view));
        }
        executorService3.shutdown();
        executorService3.awaitTermination(10, TimeUnit.SECONDS);

        view.printYellowMessage("ScheduledThreadPool\n");
        ExecutorService executorService4 = java.util.concurrent.Executors.
                newScheduledThreadPool(numberOfThreads);
        for (int i = 0; i < numberOfThreads; i++) {
            executorService4.submit(new MyRunnable(view));
        }
        executorService4.shutdown();
        executorService4.awaitTermination(10, TimeUnit.SECONDS);

        view.printYellowMessage("WorkStealingPool\n");
        ExecutorService executorService5 = java.util.concurrent.Executors.
                newWorkStealingPool();
        for (int i = 0; i < numberOfThreads; i++) {
            executorService5.submit(new MyRunnable(view));
        }
        executorService5.shutdown();
        executorService5.awaitTermination(10, TimeUnit.SECONDS);

    }
}

class MyRunnable implements Runnable {

    private View view;

    MyRunnable(View view) {
        this.view = view;
    }

    @Override
    public void run() {
        view.printMessage(
                Thread.currentThread().getName() + " -> \n"
                        + (new Fibonacci(new Random().nextInt(40)).toString()));
    }
}

