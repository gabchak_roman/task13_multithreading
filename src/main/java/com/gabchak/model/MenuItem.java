package com.gabchak.model;

public interface MenuItem {
    int NUMBER_OF_THREADS = 4;
    String getTitle();
    String getKey();
    void execute();
}
